# HMI2MID

> Convert HMI files to standard MIDIs

Based on the HMI2MID code found in Daggerfall Jukebox,
(<https://www.dfworkshop.net/downloads/old-tools/daggerfall-jukebox/>), which is
itself based on the WinRipper HMI to MID functions by Peter Palowski
(<http://www.blorp.com/~peter/>)
