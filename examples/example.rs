extern crate hmi2mid;

use hmi2mid::Hmi2mid;

fn main() {
  use std::io::{Read, Write};

  let hmipaths : Vec <std::path::PathBuf> = std::env::args().skip(1)
    .map(std::path::PathBuf::from).collect();
  if hmipaths.is_empty() {
    println!("Missing required argument: 1 or more paths to HMI files");
    std::process::exit(-1);
  }

  let mut hmi2mid = Hmi2mid::new();

  for hmipath in hmipaths.into_iter() {
    let midpath = {
      let mut m = hmipath.clone();
      m.set_extension("mid");
      m
    };
    let hmi_bytes = {
      let mut f = std::fs::File::open(hmipath).unwrap();
      let mut v = Vec::new();
      let _ = f.read_to_end(&mut v).unwrap();
      v
    };
    let mid_bytes = hmi2mid.hmi_rip(hmi_bytes).unwrap();
    let mut f = std::fs::File::create(midpath).unwrap();
    let _ = f.write_all (&mid_bytes).unwrap();
  }
}
